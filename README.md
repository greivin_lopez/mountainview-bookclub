# Mountain View High School - Book Club #

_Description:_ This is a web app for Mountain View High School. To allow students to register the days they read a book.

This is how the website looks like:
![Home Page](http://content.screencast.com/users/GreivinLopez/folders/Jing/media/65d4233f-3abc-4b4a-b357-623273f3d945/00000015.png)

## Technology Used

The solution was created using [Google App Engine](https://developers.google.com/appengine) with the Python SDK.

Users login/logout functionality is based on Official Google Accounts. So the user needs to be logged to their Google Account, otherwise the app will redirect the user to the Google login site.

For the RESTful API I used the [Skuë Library](https://bitbucket.org/greivin_lopez/skue). An open source library that I created to allow GAE developers to create good RESTful web services.

**Note:** Skuë means "ratón"/"mouse" in Bribrí (indigenous costa rican language). 

## Running the app

The website was hosted on Google App Engine infrastructure. To access the website just open your browser at this URL:

http://mountainview-bookclub.appspot.com/

## Status

**The solution isn't complete.** It lacks the feature of entering a book name and interacting with the calendar so it stores the "read event" on the server.

## How to store tests directly to the server?

To test the rendering of the events you can use the provided RESTful API by performing a **POST** request to this URL: http://mountainview-bookclub.appspot.com/api/users/**<user-id>**/events.

You have to replace the **<user-id>** with your actual user Id. To know your user ID you can retrieve it from the web browser address by clicking on your user name at the home page. See the instructions on this screencast: [How to know my user Id](http://screencast.com/t/puiq8lNFTgnW).

For the actual POST request to the API you can follow the instructions on this screencast: [POST request using Advanced Rest Client Chrome Plugin](http://screencast.com/t/tLMSzaxCnqg6).

Once the POST succeeded you can go to your user calendar to see the stored book read event:

![Read Event](http://content.screencast.com/users/GreivinLopez/folders/Jing/media/f7d70381-6b31-4c1f-9823-d744776eaecf/00000017.png)