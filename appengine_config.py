#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 12, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Python built-in modules *****
import os

# ***** Google App Engine imports *****
from google.appengine.api import namespace_manager

def namespace_manager_default_namespace_for_request():
    """Determine which namespace is to be used for a request."""
    return os.environ.get('NAMESPACE_ENVIRONMENT', '')
