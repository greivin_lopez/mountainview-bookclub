#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 13, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Python built-in modules *****


# ***** Google App Engine imports *****


# ***** Application modules *****
from app.models.models import ReadEvent, ClubUser
from app.response.json.events import ReadEventCollectionJSON, ReadEventJSON
from skue.rest.api import ResourceDescription, HttpMethodExample,\
    HttpRequestExample, HttpResponseExample, STATUS_CODES
from skue.rest.api import HttpMethodDescription
from skue.rest.api import HttpParameterDescription
from skue.http import CommonResponse
from skue.rest.resources import CollectionResource
from skue.rest.decorators import ensure, response
from skue.rest.api import RepresentationType as ContentType


__author__ = u"Greivin Lopez"
__copyright__ = u"Copyright 2014, Salsamobi"
__credits__ = [u"Greivin Lopez"]
__license__ = "Proprietary"
__version__ = "1"
__maintainer__ = u"Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# ReadEventsCollection
#===============================================================================
class ReadEventsCollection(CollectionResource):
    """Represents a collection of book read events."""
    def describe_resource(self):
        """Self description of this resource handler"""
        
        #==================
        #  POST            
        #==================        
        post_example_response_result = u"""
{
    "status": "OK",
    "message": "Successfully created",
    "uri": "/api/users/07b24695f346383d9e82c447656ece74b55d18ca/events"
}
"""

        post_method = HttpMethodDescription('POST',
                                            parameters=[
                                                        HttpParameterDescription('book',
                                                                                 'string',
                                                                                 is_required=True,
                                                                                 description="The name of the book"),
                                                        HttpParameterDescription('start',
                                                                                 'string',
                                                                                 is_required=True,
                                                                                 description="The day the book was read"),
                                                       ],
                                            description="Inserts a new read event",
                                            example=HttpMethodExample(request=HttpRequestExample(url='http://mountainview-bookclub.appspot.com/api/users/07b24695f346383d9e82c447656ece74b55d18ca/events',
                                                                                                 payload='name=The Great Gatsby&start=2014-06-13'),
                                                                      response=HttpResponseExample(result=post_example_response_result,
                                                                                                   success_status=STATUS_CODES[201])))
        
        #==================
        #  GET            
        #==================
        get_example_response_result = u"""

        """

        get_method = HttpMethodDescription('GET',
                                            parameters=[],
                                            description="Gets the complete list of read events for a given user",
                                            example=HttpMethodExample(request=HttpRequestExample(url='http://mountainview-bookclub.appspot.com/api/users/07b24695f346383d9e82c447656ece74b55d18ca/events',
                                                                                                 payload='Not applicable'),
                                                                      response=HttpResponseExample(result=get_example_response_result,
                                                                                                   success_status=STATUS_CODES[200])))

        resource = ResourceDescription('Read Events Collection',
                                        url="/api/users/<id>/events",
                                        methods=[post_method, get_method],
                                        description="Represents the collection of read events for a particular user")
        return resource
    
    @ensure(required=['book', 'start'])
    @response(representations=[ContentType.JSON])
    def create_resource(self, user_id, **arguments):
        """Create a new read event
 
        Returns:
          A resource created response (201).
        """
        book = arguments['book'] if 'book' in arguments else None
        start = arguments['start'] if 'start' in arguments else None

        event = ReadEvent.create(book, start, user_id)
        resource_uri = ''.join([self.request.path, '/'])
        return CommonResponse.resource_created(resource_uri=resource_uri)
    
    @response(representations=[ContentType.JSON])
    def read_resource(self, user_id, **arguments):
        """Gets the read events for a given user"""
        user = ClubUser.get_by_id(user_id)
        if user is None:
            return CommonResponse.resource_not_found()  
        events = user.get_events()

        body = ReadEventCollectionJSON(events)
        return CommonResponse.success(body)

