#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 12, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Python built-in modules *****
from random import choice
import webapp2
from webapp2_extras import jinja2

# ***** Google App Engine imports *****
from google.appengine.api import users

# ***** Application modules *****
from app.models.models import ClubUser
from app.utils.constants import COPYRIGHT_YEAR, QUOTES


__author__ = u"Greivin Lopez"
__copyright__ = u"Copyright 2014, Salsamobi"
__credits__ = [u"Greivin Lopez"]
__license__ = "Proprietary"
__version__ = "1"
__maintainer__ = u"Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# WebPageBaseHandler
#===============================================================================
class WebPageBaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(app=self.app)

    def render_response(self, template, **context):
        # Checks for active Google account session
        google_user = users.get_current_user()

        if not google_user:
            self.redirect(users.create_login_url(self.request.uri))
            return

        # Lazyly create the system user
        user = ClubUser.get_by_google_id(google_user.user_id())
        if not user:
            user = ClubUser.create(google_user)
        
        # Renders a template and writes the result to the response.
        rendered_response = self.jinja2.render_template(template,
                                                        copyright_year=COPYRIGHT_YEAR,                                                        
                                                        user=user.name,
                                                        userid=user.key.id(),
                                                        logout_url=users.create_logout_url("/"),
                                                        return_page=self.request.url,                                                        
                                                        **context)
        self.response.out.write(rendered_response)


#===============================================================================
# HomePage
#===============================================================================
class HomePage(WebPageBaseHandler):
    """Handler for the web home page"""

    def get(self):
        quote = choice(QUOTES)
        template_values = { 
                           'quote': quote,
                          }
        self.render_response('home.html', **template_values)

#===============================================================================
# CalendarPage
#===============================================================================
class CalendarPage(WebPageBaseHandler):
    """Handler for the calendar page"""

    def get(self, user_id):
        template_values = {
                        }
        self.render_response('calendar.html', **template_values)
