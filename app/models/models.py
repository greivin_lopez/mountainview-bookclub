#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 13, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Python built-in modules *****
import hashlib

# ***** Google App Engine imports *****
from google.appengine.ext import ndb

# ***** Application modules *****
from app.utils.constants import SALT


__author__ = u"Greivin Lopez"
__copyright__ = u"Copyright 2014, Salsamobi"
__credits__ = [u"Greivin Lopez"]
__license__ = "Proprietary"
__version__ = "1"
__maintainer__ = u"Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# ClubUser
#===============================================================================
class ClubUser(ndb.Model):
    """Entity representation of a book club user"""
    name = ndb.StringProperty(required=True)
    email = ndb.StringProperty(required=True)
    google_user_id = ndb.StringProperty()
    created = ndb.DateTimeProperty(auto_now_add=True)

    @classmethod
    def create(cls, google_user):
        """Creates a new user with the given Google user"""
        identifier = cls.create_id(google_user.email())
        user = ClubUser(id=identifier,
                        name=google_user.nickname(),
                        email=google_user.email(),
                        google_user_id=google_user.user_id())
        user.put()
        return user

    @classmethod
    def create_id(cls, email):
        """Creates a user identifier from the given email
        
        Args:
            email: An email address
        
        Returns:
            A SHA1 value generated from the given email.
        """
        user_id = ''.join([email.lower(), SALT])
        identifier = hashlib.sha1(user_id).hexdigest()
        return identifier

    @classmethod
    def get_by_google_id(cls, google_id):
        """Gets a user by the google id if the account is linked. Return None
        otherwise."""
        return ClubUser.query(ClubUser.google_user_id == google_id).get()

    def get_events(self):
        query = ReadEvent.query(ReadEvent.user == self.key)
        return query.fetch(100)


#===============================================================================
# ReadEvent
#===============================================================================
class ReadEvent(ndb.Model):
    """Entity representation of a read event. A user reading a book at a certain day."""
    book = ndb.StringProperty(required=True)
    allDay = ndb.BooleanProperty(default=True)
    start = ndb.StringProperty(required=True)
    user = ndb.KeyProperty(ClubUser)

    @classmethod
    def create(cls, book, start, user_id):
        """Creates a new read event"""
        user = ndb.Key(ClubUser, user_id)

        event = ReadEvent(book=book,
                        start=start,
                        user=user)
        event.put()
        return event




