#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 13, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Application modules *****
from skue.json.utils import ResourceJSONRepresentation

__author__ = u"Greivin Lopez"
__copyright__ = u"Copyright 2014, Salsamobi"
__credits__ = [u"Greivin Lopez"]
__license__ = "Proprietary"
__version__ = "1"
__maintainer__ = u"Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


#===============================================================================
# ReadEventJSON
#===============================================================================
class ReadEventJSON(ResourceJSONRepresentation):
    """JSON representation for a reading event"""
    def __init__(self, event):
        ResourceJSONRepresentation.__init__(self, 'Read Event')
        self.title = event.book
        self.allDay = event.allDay
        self.start = event.start


#===============================================================================
# ReadEventCollectionJSON
#===============================================================================
class ReadEventCollectionJSON(ResourceJSONRepresentation):
    """JSON representation of a collection of read events"""
    def __init__(self, events):
        ResourceJSONRepresentation.__init__(self, 'Read Events Collection')
        self.events = [ReadEventJSON(event) for event in events]