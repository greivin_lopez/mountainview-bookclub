#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 12, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Python built-in modules *****
import os
from datetime import date


__author__ = u"Greivin Lopez"
__copyright__ = u"Copyright 2014, Salsamobi"
__credits__ = [u"Greivin Lopez"]
__license__ = "Proprietary"
__version__ = "1"
__maintainer__ = u"Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


RUNNING_LOCAL = os.environ.get('NAMESPACE_ENVIRONMENT') == 'local'

TESTING = os.environ.get('NAMESPACE_ENVIRONMENT') == 'qa'

COPYRIGHT_YEAR = "2014" if date.today().year <= 2014 else "2014-%s" % date.today().year

SALT = "zKhbB3DLJXaBcnQEffH4rSMu8z5UXZ5i7gf2ctvY"

QUOTES = [
          {
           'quote': u"A reader lives a thousand lives before he dies, said Jojen. The man who never reads lives only one.",
           'author': u"George R.R. Martin"
          },
          {
           'quote': u"I read books because I love them, not because I think I should read them.",
           'author': u"Simon Van Booy"
          },
          {
           'quote': u"You're never alone when you're reading a book.",
           'author': u"Susan Wiggs"
          },
          {
           'quote': u"Life is too short to read books that I'm not enjoying.",
           'author': u"Melissa Marr"
          },
          {
           'quote': u"Clearly one must read every good book at least once every ten years.",
           'author': u"C.S. Lewis"
          },
          {
           'quote': u"I read like the flame reads the wood.",
           'author': u"Alfred Döblin"
          },
          {
           'quote': u"People who say they don't have time to read simply don't want to.",
           'author': u"Julie Rugg"
          },
          {
           'quote': u"Never read a book that is not a year old.",
           'author': u"Ralph Waldo Emerson"
          },
          {
           'quote': u"Before this generation lose the wisdom, one advice - read books.",
           'author': u"Amit Kalantri"
          },
          {
           'quote': u"Books are a staircase to unknown worlds.",
           'author': u"Jason Ellis"
          },
          {
           'quote': u"Reading doesn't mean accepting everything you read, it means reasoning everything you read.",
           'author': u"Amit Kalantri"
          },
          {
           'quote': u"Take some books and read; that’s an immense help; and books are always good company if you have the right sort.",
           'author': u"Louisa May Alcott"
          },
          {
           'quote': u"I read everyday, because reading takes me away, away to a place where nothing is impossible.",
           'author': u"Manoj Arora"
          },
          {
           'quote': u"If you are not reading and thinking, it means that your windows looking to the ocean are closed!",
           'author': u"Mehmet Murat ildan"
          },
          {
           'quote': u"Only the very weak-minded refuse to be influenced by literature and poetry.",
           'author': u"Cassandra Clare"
          },
          {
           'quote': u"Literature is a textually transmitted disease, normally contracted in childhood.",
           'author': u"Jane Yolen"
          },
          {
           'quote': u"A classic is a book that has never finished saying what it has to say.",
           'author': u"Italo Calvino"
          },
          {
           'quote': u"The reading of all good books is like conversation with the finest men of past centuries.",
           'author': u"René Descartes"
          },
          {
           'quote': u"A good book is an event in my life.",
           'author': u"Stendhal"
          },
          {
           'quote': u"Literature is my Utopia.",
           'author': u"Helen Keller"
          },
          {
           'quote': u"Books are like imprisoned souls till someone takes them down from a shelf and frees them.",
           'author': u"Samuel Butler"
          },
          {
           'quote': u"In great literature, I become a thousand different men but still remain myself.",
           'author': u"C.S. Lewis"
          },
          {
           'quote': u"Literature can remind us that not all life is already written down: there are still so many stories to be told.",
           'author': u"Colum McCann"
          },
          {
           'quote': u"My definition of good literature is that which can be read by an educated reader, and reread with increased pleasure.",
           'author': u"Gene Wolfe"
          },
          {
           'quote': u"High and fine literature is wine, and mine is only water; but everybody likes water.",
           'author': u"Mark Twain"
          },
          {
           'quote': u"I like my coffee with cream and my literature with optimism.",
           'author': u"Abigail Reynolds"
          },
          {
           'quote': u"The answers you get from literature depend on the questions you pose.",
           'author': u"Margaret Atwood"
          },
          {
           'quote': u"I had found my religion: nothing seemed more important to me than a book. I saw the library as a temple.",
           'author': u"Jean-Paul Sartre"
          },
          {
           'quote': u"The pleasures of writing correspond exactly to the pleasures of reading.",
           'author': u"Vladimir Nabokov"
          },          
         ]
