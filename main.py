#!/usr/bin/env python
# -*- coding: utf8 -*-
'''
Created on Jun 12, 2014

@author: Greivin Lopez
@copyright: Salsamobi
'''
# ***** Python built-in modules *****

# ***** Google App Engine imports *****
import webapp2

# ***** Application modules *****
from app.resources.events import ReadEventsCollection
from app.web.pages import HomePage, CalendarPage


__author__ = u"Greivin Lopez"
__copyright__ = u"Copyright 2014, Salsamobi"
__credits__ = [u"Greivin Lopez"]
__license__ = "Proprietary"
__version__ = "1"
__maintainer__ = u"Greivin Lopez"
__email__ = "greivin.lopez@gmail.com"
__status__ = "Development"


TASK_HANDLERS = [
                    
                ]

##############
# Public API #
##############
API_HANDLERS = [
                   ('/api/users/(.*)/events', ReadEventsCollection),     
               ]

###############
# Private API #
###############
PRIVATE_HANDLERS = [
                        
                   ]

HTML_HANDLERS = [
                    ('/', HomePage),
                    ('/users/(.*)/events', CalendarPage),
                ]

ADMIN_HANDLERS = [
                    
                 ]

ROUTES = TASK_HANDLERS + API_HANDLERS + PRIVATE_HANDLERS + HTML_HANDLERS + ADMIN_HANDLERS

config = {}
config['webapp2_extras.sessions'] = {
    'secret_key': 'TMyNH2KDssqel5KEuI7vHgH5yMpEi5sSE7WuM1Zj20cGVDvQ4pWgYfUwitV3'
}
app = webapp2.WSGIApplication(ROUTES, config=config, debug=True)
